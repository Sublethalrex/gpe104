﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class RobotPawn : Pawn {

	private Rigidbody2D rb;
	private SpriteRenderer sr;
	private Transform tf;
	private Animator anim;
    private int cjump;
    private Transform spawn;

    public float moveSpeed;
	public float jumpForce;
	public bool isGrounded;
	public float groundDistance = 0.1f;
    public int multijump;
    public int numrespawn;
    public bool touching;
    

	public void Start() {
		// Get my components
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
		anim = GetComponent<Animator> ();
	}

	public void Update() {
		// Check for Grounded
		CheckForGrounded(); 
        
        
	}


   


	public void CheckForGrounded () {
        RaycastHit2D[] hits; //raycasts to see if we're grounded

        
        Vector2 positionToCheck = transform.position;
        hits = Physics2D.RaycastAll(positionToCheck, new Vector2(0, -1), 2f);

        
        if (hits.Length > 1)
        {
            isGrounded = true;
            cjump = multijump;
        }
        else {
            isGrounded = false;
        }
    }

    void OnCollisionEnter2D(Collision2D hit) //handles collision with damage dealer or checkpoints
    {
        
        if (hit.gameObject.tag == "Death")
        {
            touching = true;
            respawn();
            numrespawn = numrespawn - 1;
  
            
        }
        else if (hit.gameObject.tag == "Spawn") {
            spawn = hit.transform;
            touching = true;
        }

    }
    public void respawn() { //respawn function to respawn at previous checkpoint
        if (numrespawn > 0)
        {
            transform.position = spawn.position;
        }
        else if (numrespawn <= 0) {
            SceneManager.LoadScene("failed");
        }
    }

	public override void Move (Vector2 moveVector) {
		// Change X velocity based on speed and moveVector
		rb.velocity = new Vector2 (moveVector.x * moveSpeed, rb.velocity.y);

        // If velocity is <0, flip sprite.

        if (rb.velocity.x < 0)
        {
            anim.Play("RobotWalk");
            sr.flipX = true;
        }
        else if (rb.velocity.x > 0)
        {
            anim.Play("RobotWalk");
            sr.flipX = false;
        }
       

        else
        {
            anim.Play("RobotIdle");
        }
	}

	public override void Jump () { //handles normal and double jumps

        if (isGrounded == true) { //normal grounded jumps

            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
            
        }

		
       if (isGrounded == false && cjump != 0) { //double or more jumps

                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Force);
                cjump = cjump - 1;
            
        }
			

		
	}

}
