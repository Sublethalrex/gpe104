﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movemoent : MonoBehaviour {
    public float speed;
    private bool moveable;
    public bool shift;
	// Use this for initialization
	void Start () {
        shift = false;
		
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.P))
        {
            moveable = !moveable; //stops the ability to move the sprite 
        }
        if (Input.GetKeyDown(KeyCode.LeftShift)) {// was supposed to handle getting the shift key for one time use but the thing one toggle it it just stays on
            shift = true;
        }

        if  (shift == true);        //handles the one time movement however its broken and i dont know how to fix it
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                transform.position += Vector3.left * 10;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                transform.position += Vector3.right * 10;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                transform.position += Vector3.up * 10;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                transform.position += Vector3.down * 10;
            }
            shift = false;

            

        }
        if (moveable == true) {     //standard movement controllable in the inspector 
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.position += Vector3.left * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.position += Vector3.right * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                transform.position += Vector3.up * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.position += Vector3.down * speed * Time.deltaTime;
            }

        }
       

        
        
    }
}
