﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTank : MonoBehaviour
{


    public Transform tf;
    public float speed;
    public float turnspeed;

    // Use this for initialization
    void Start()
    {

        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.W))
        {
            tf.position += tf.up * speed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            tf.Rotate(0, 0, -turnspeed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            tf.Rotate(0, 0, turnspeed);
        }

        if (Input.GetKey(KeyCode.S))
        {
            tf.position += tf.up * -speed;
        }


    }
}
