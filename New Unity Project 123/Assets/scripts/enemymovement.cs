﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemymovement : MonoBehaviour {


   public Transform targetTf;
    private Transform tf;
    public float speed;
    Vector3 movementvector;



    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();
        
        movementvector = targetTf.position - tf.position;
        movementvector.Normalize();
        movementvector = movementvector * speed;
        tf.position = tf.position + movementvector;
        tf.right = movementvector;

    }
	
	// Update is called once per frame
	void Update () {



        tf.position = tf.position + movementvector;



    }
}
