﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {
    public Transform tf;
    public float speed;
   

    // Use this for initialization
    void Start()
    {

        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {

        
            tf.position += tf.up * speed;
        


    }
}
