﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {


    public GameObject[] obj;
    public float spawnmin = 1f;
    public float spawnmax = 2f;

	// Use this for initialization
	void Start () {
        Spawn();
	}

    void Spawn() {

        Instantiate(obj[Random.Range(0, obj.Length)], transform.position, Quaternion.identity);
        Invoke("Spawn", Random.Range(spawnmin, spawnmax));
    }
}
