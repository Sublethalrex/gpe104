﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour {

    public GameObject enemyprefab;
    public GameObject[] enemies;
    public int  maxenemy = 3;
    public GameObject[] spawners;
    private int cenemy;
    private int cspawner;
    private Transform spawntransform;
	public int chanceoffollowermax;
	private int chanceoffollower;
	public GameObject Follower;
    
    


	// Use this for initialization
	void Start () {

        

    }
	
	// Update is called once per frame
	void Update () {
        
        enemies = GameObject.FindGameObjectsWithTag("asteroid");
        cenemy = enemies.Length;

        if (cenemy < maxenemy) {

			chanceoffollower = Random.Range (1, chanceoffollowermax);

			if (chanceoffollower == 1) {

				cspawner = Random.Range (0, spawners.Length);
				spawntransform = spawners [cspawner].transform;
				Instantiate (Follower, spawntransform.position, spawntransform.rotation);

			} 
			else {
			
				cspawner = Random.Range(0, spawners.Length);
				spawntransform = spawners[cspawner].transform;
				Instantiate(enemyprefab, spawntransform.position, spawntransform.rotation);
			
			}

            
            
        }

        enemies[0] = null;
        enemies[1] = null;
        enemies[2] = null;





    }
}
