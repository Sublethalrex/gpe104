﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class Follow : MonoBehaviour {

    public Transform targetTf;
    private Transform tf;
    public float speed;
	private GameObject player;




    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();


	}
	
	// Update is called once per frame
	void Update () {

        Vector3 movementvector;
        movementvector = targetTf.position - tf.position;
        movementvector.Normalize();
        movementvector = movementvector * speed;
        tf.position = tf.position + movementvector;
        tf.up = movementvector;

		
	}
}
