﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : Controller {
	public KeyCode shoutKey;
	public KeyCode upKey;
	public KeyCode downKey;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (upKey)) {
			pawn.Move (Vector3.up * 0.01f);
		}
		if (Input.GetKey (downKey)) {
			pawn.Move (Vector3.up * -0.01f);
		}

		if (Input.GetKeyDown (shoutKey)) {
			pawn.Shout ();
		}
	}
}
