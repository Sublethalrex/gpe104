﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

	// Tell C# that all Pawns can shout, and they all do it the same way
	public void Shout() {
		Debug.Log ("AAAAaaaaaAAAhh!");
	}

	// Tell C# that all Pawns will have this function, BUT, we expect to override it in the child pawns
	public virtual void Move (Vector3 direction) {
	}

}
