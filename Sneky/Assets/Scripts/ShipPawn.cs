﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipPawn : Pawn {

	public override void Move (Vector3 direction) {
		GetComponent<Transform> ().position += direction;		
	}
}
